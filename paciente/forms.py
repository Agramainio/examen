from django import forms
from django.db import models
class RegPa(forms.Form):
    rut = models.CharField(max_length=11, blank=True, null=True)
    nombre = models.CharField(max_length=80, blank=True, null=True)
    fechaNacimiento = models.DateTimeField()