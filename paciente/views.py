from django.shortcuts import render
from .forms import RegPa
from .models import Paciente
def inicio(request):
    form = RegPa(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        rut2 = form_data.get("rut")
        nombre2 = form_data.get("nombre")
        fechan2 = form_data.get("fechaNacimiento")
        objeto = Paciente.objects.create(rut=rut2, nombre=nombre2, fechaNacimiento=fechan2)
    contexto = {
        "el_paciente": form,
    }
    return render(request,"paciente.html", contexto)
# Create your views here.
