from django.db import models

# Create your models here.
class Paciente(models.Model):
    rut = models.CharField(max_length=11, blank=True,null=True)
    nombre = models.CharField(max_length=80, blank=True,null=True)
    fechaNacimiento = models.DateTimeField()
    fechaIngreso =models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.rut
