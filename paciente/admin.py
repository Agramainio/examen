from django.contrib import admin
from paciente.models import *

class AdminPaciente(admin.ModelAdmin):
    list_display = ["rut","nombre", "fechaNacimiento", "fechaIngreso"]
    list_filter = ["fechaIngreso"]
    list_editable = ["nombre"]
    search_fields = ["nombre", "rut"]
    class Meta:
        model = Paciente

# Register your models here.
admin.site.register(Paciente, AdminPaciente)